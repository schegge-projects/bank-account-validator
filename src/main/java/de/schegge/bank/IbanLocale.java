package de.schegge.bank;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class IbanLocale {
    private static final Map<String, IbanLocale> IBAN_LOCALES = new HashMap<>();

    public enum Type {
        SEPA,
        NO_SEPA,
        EXPERIMENTAL
    }

    private static final Set<IbanLocale> SEPA = new HashSet<>();
    public static final IbanLocale GERMANY;
    public static final IbanLocale FRANCE;

    public static final IbanLocale ITALY;

    static {
        GERMANY = createConstant("DE", Type.SEPA, 22);
        FRANCE = createConstant("FR", Type.SEPA, 27);
        ITALY = createConstant("IT", Type.SEPA, 27);
        try {
            Properties properties = new Properties();
            properties.load(IbanLocale.class.getResourceAsStream("ibanLocales.properties"));
            properties.forEach((code, value) -> createConstant(String.valueOf(code), String.valueOf(value)));
            IBAN_LOCALES.values().stream().filter(IbanLocale::isSepa).forEach(SEPA::add);
        } catch (RuntimeException | IOException e) {
            throw new ExceptionInInitializerError("cannot find iban locales");
        }
    }

    private static void createConstant(String code, String value) {
        String[] parts = value.split(",");
        createConstant(code, Type.valueOf(parts[0]), Integer.parseInt(parts[1]));
    }

    private static IbanLocale createConstant(String country, Type type, int length) {
        return IBAN_LOCALES.computeIfAbsent(country, c -> new IbanLocale(country, type, length));
    }

    public static IbanLocale byCode(Locale country) {
        return IBAN_LOCALES.get(country.getCountry());
    }

    public static IbanLocale byCode(String country) {
        return IBAN_LOCALES.get(country);
    }

    public static Set<IbanLocale> allIbanLocales() {
        return Set.copyOf(IBAN_LOCALES.values());
    }

    public static Set<IbanLocale> allSepaIbanLocales() {
        return Set.copyOf(SEPA);
    }

    private final String country;
    private final Type type;
    private final int length;

    public IbanLocale(String country, Type type, int length) {
        this.country = country;
        this.type = type;
        this.length = length;
    }

    public String getCountry() {
        return country;
    }

    public boolean isSepa() {
        return type == Type.SEPA;
    }

    public boolean isExperimental() {
        return type == Type.EXPERIMENTAL;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "iban country-code=" + country + ", type=" + type + ", length=" + length;
    }
}
