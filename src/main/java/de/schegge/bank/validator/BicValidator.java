package de.schegge.bank.validator;

import de.schegge.bank.BankService;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Optional;import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BicValidator implements ConstraintValidator<BIC, String> {

    private static final Pattern PATTERN = Pattern.compile("([A-Z0-9]{4})([A-Z]{2})([A-Z0-9]{2})([A-Z0-9]{3})?");

    private Check check;

    @Override
    public void initialize(BIC constraintAnnotation) {
        this.check = constraintAnnotation.check();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }
        Matcher matcher = PATTERN.matcher(value);
        if (!matcher.matches()) {
            return false;
        }
        if (check == Check.LENIENT) {
            return true;
        }
        Optional<BankService> bankService = BankService.byCountry(matcher.group(2));
        if (bankService.isEmpty()) {
            return check == Check.PRAGMATIC;
        }
        return bankService.map(service -> service.byBankIdentifierCode(value)).filter(x -> !x.isEmpty()).isPresent();
    }
}
