package de.schegge.bank.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface IBAN {
    enum IbanType {
        ALL, SEPA, NO_SEPA
    }

    String message() default "{de.schegge.bank.validator.IBAN.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Check check() default Check.PRAGMATIC;

    IbanType type() default IbanType.ALL;
}
