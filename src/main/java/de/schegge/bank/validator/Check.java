package de.schegge.bank.validator;

public enum Check {
    LENIENT,
    PRAGMATIC,
    STRICT
}
