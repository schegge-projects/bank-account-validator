package de.schegge.bank.service;

import de.schegge.bank.Bank;
import de.schegge.bank.BankService;
import de.schegge.bank.CountryCode;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CountryCode("DE")
public class GermanBankServiceImpl extends BankService {
    public static final String FILE_NAME = "BLZ.txt";
    private final Map<String, List<Bank>> byBankIdentifierCode;
    private final Map<String, Bank> byBankCode;

    public GermanBankServiceImpl() {
        List<Bank> banks = getResourceAsLines(FILE_NAME).map(GermanBankServiceImpl::convert).toList();
        byBankCode = banks.stream().collect(Collectors.toMap(Bank::bankCode, b -> b, (a, b) -> a));
        byBankIdentifierCode = banks.stream().collect(Collectors.groupingBy(Bank::bankIdentifierCode));
    }

    private static Bank convert(String x) {
        String name = x.substring(9, 67).trim();
        String bankIdentifierCode = x.substring(139, 150);
        String blz = x.substring(0, 8);
        return new Bank(name, blz, bankIdentifierCode);
    }

    @Override
    public List<Bank> byBankIdentifierCode(String bankIdentifierCode) {
        return byBankIdentifierCode.getOrDefault(bankIdentifierCode.length() == 8 ? bankIdentifierCode + "XXX" : bankIdentifierCode, List.of());
    }

    @Override
    public Bank byBankCode(String bankCode) {
        return byBankCode.get(bankCode);
    }

    @Override
    public Bank byBasicBankAcountNumber(String basicBankAccountNumber) {
        return byBankCode(basicBankAccountNumber.substring(0, 8));
    }
}
