package de.schegge.bank.service;

import de.schegge.bank.Bank;
import de.schegge.bank.BankService;
import de.schegge.bank.CountryCode;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CountryCode("AT")
public class AustriaBankServiceImpl extends BankService {
    public static final String FILE_NAME = "sepa-zv-vz_gesamt.csv";
    private final Map<String, List<Bank>> byBankIdentifierCode;
    private final Map<String, Bank> byBankCode;

    public AustriaBankServiceImpl() {
        List<Bank> banks = getResourceAsLines(FILE_NAME).skip(1L).map(AustriaBankServiceImpl::convert).toList();
        byBankCode = banks.stream().collect(Collectors.toMap(Bank::bankCode, b -> b, (a, b) -> a));
        byBankIdentifierCode = banks.stream().collect(Collectors.groupingBy(Bank::bankIdentifierCode));
    }

    private static Bank convert(String x) {
        String[] parts = x.split(";");
        return new Bank(parts[6], parts[2], parts[18]);
    }

    @Override
    public List<Bank> byBankIdentifierCode(String bankIdentifierCode) {
        return byBankIdentifierCode.getOrDefault(bankIdentifierCode.length() == 8 ? bankIdentifierCode + "XXX" : bankIdentifierCode, List.of());
    }

    @Override
    public Bank byBankCode(String bankCode) {
        return byBankCode.get(bankCode);
    }

    @Override
    public Bank byBasicBankAcountNumber(String basicBankAccountNumber) {
        return byBankCode(basicBankAccountNumber.substring(0, 5));
    }
}
