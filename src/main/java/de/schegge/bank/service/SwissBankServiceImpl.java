package de.schegge.bank.service;

import de.schegge.bank.Bank;
import de.schegge.bank.BankService;
import de.schegge.bank.CountryCode;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@CountryCode("CH")
public class SwissBankServiceImpl extends BankService {
    private static final String FILE_NAME = "bcbankenstamm.txt";

    private final Map<String, List<Bank>> byBankIdentifierCode;
    private final Map<String, Bank> byBankCode;

    public SwissBankServiceImpl() {
        List<Bank> banks = getResourceAsLines(FILE_NAME).map(SwissBankServiceImpl::convert).toList();
        byBankCode = banks.stream().collect(Collectors.toMap(Bank::bankCode, b -> b, (a, b) -> a));
        byBankIdentifierCode = banks.stream().collect(Collectors.groupingBy(Bank::bankIdentifierCode));
    }

    private static Bank convert(String x) {
        String name = x.substring(39, 54).trim();
        String bankIdentifierCode = x.substring(x.length() - 14).trim();
        return new Bank(name, getIid(x, 11).or(() -> getIid(x, 2)).orElseThrow(), bankIdentifierCode);
    }

    private static Optional<String> getIid(String x, int offset) {
        return Optional.of(x.substring(offset, offset + 5)).map(String::trim).filter(Predicate.not(String::isEmpty)).map(Integer::parseInt).map("%05d"::formatted);
    }

    @Override
    public List<Bank> byBankIdentifierCode(String bankIdentifierCode) {
        return byBankIdentifierCode.getOrDefault(bankIdentifierCode.length() == 8 ? bankIdentifierCode + "XXX" : bankIdentifierCode, List.of());
    }

    @Override
    public Bank byBankCode(String bankCode) {
        return byBankCode.get(bankCode);
    }

    @Override
    public Bank byBasicBankAcountNumber(String basicBankAccountNumber) {
        return byBankCode(basicBankAccountNumber.substring(0, 5));
    }
}
