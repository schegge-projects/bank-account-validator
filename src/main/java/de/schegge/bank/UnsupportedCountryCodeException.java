package de.schegge.bank;

public class UnsupportedCountryCodeException extends IllegalArgumentException {
    public UnsupportedCountryCodeException(String country) {
        super(String.valueOf(country));
    }
}
