package de.schegge.bank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public abstract class BankService {
    private record BankServiceWithCountry(String code, BankService bankService) {

    }

    private static final Map<String, BankService> BANK_SERVICES = ServiceLoader.load(BankService.class).stream().map(BankService::convert)
            .filter(Objects::nonNull).collect(toMap(BankServiceWithCountry::code, BankServiceWithCountry::bankService));

    private static BankServiceWithCountry convert(Provider<BankService> bankService) {
        return Optional.ofNullable(bankService.type().getAnnotation(CountryCode.class)).map(CountryCode::value)
                .map(x -> new BankServiceWithCountry(x, bankService.get())).orElse(null);
    }

    public static Optional<BankService> byCountry(Locale country) {
        return byCountry(Objects.requireNonNull(country).getCountry());
    }

    public static Optional<BankService> byCountry(String country) {
        BankService bankService = BANK_SERVICES.get(Objects.requireNonNull(country));
        return Optional.ofNullable(bankService);
    }

    public static BankService getDefault() {
        String country = Locale.getDefault().getCountry();
        return byCountry(country).orElseThrow(() -> new UnsupportedCountryCodeException(country));
    }

    public Map<String, BankService> availableBankServices() {
        return Collections.unmodifiableMap(BANK_SERVICES);
    }

    public abstract List<Bank> byBankIdentifierCode(String bankIdentifierCode);

    public abstract Bank byBankCode(String bankCode);

    public abstract Bank byBasicBankAcountNumber(String basicBankAccountNumber);

    protected Stream<String> getResourceAsLines(String fileName) {
        return new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(fileName), StandardCharsets.UTF_8)).lines();
    }
}
