package de.schegge.bank;

public record Bank(String name, String bankCode, String bankIdentifierCode) {

}
