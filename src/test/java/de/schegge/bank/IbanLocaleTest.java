package de.schegge.bank;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class IbanLocaleTest {

    @ParameterizedTest
    @CsvSource({
            "DE,22,true,false", "FR,27,true,false", "IT,27,true,false",
            "AD,24,false,false",
            "GQ,27,false,true"
    })
    void byCode(String country, int length, boolean isSepa, boolean isExperimental) {
        IbanLocale ibanLocale = IbanLocale.byCode(country);
        assertAll(
                () -> assertNotNull(ibanLocale),
                () -> assertEquals(country, ibanLocale.getCountry()),
                () -> assertEquals(length, ibanLocale.getLength()),
                () -> assertEquals(isSepa, ibanLocale.isSepa()),
                () -> assertEquals(isExperimental, ibanLocale.isExperimental())
        );
    }

    @Test
    void constants() {
        assertEquals(IbanLocale.GERMANY, IbanLocale.byCode("DE"));
        assertEquals(IbanLocale.FRANCE, IbanLocale.byCode("FR"));
        assertEquals(IbanLocale.ITALY, IbanLocale.byCode("IT"));
    }

    @Test
    void byLocale() {
        assertEquals(IbanLocale.GERMANY, IbanLocale.byCode(Locale.GERMANY));
        assertEquals(IbanLocale.FRANCE, IbanLocale.byCode(Locale.FRANCE));
        assertEquals(IbanLocale.ITALY, IbanLocale.byCode(Locale.ITALY));
    }

    @Test
    void all() {
        assertEquals(102, IbanLocale.allIbanLocales().size());
        assertEquals(37, IbanLocale.allSepaIbanLocales().size());
    }
}