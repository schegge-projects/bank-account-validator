package de.schegge.bank.validator;

import de.schegge.bank.validator.IBAN.IbanType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class IbanValidatorTest {
    void strict(@IBAN(check = Check.STRICT) String iban) {
        // empty
    }

    void pragmatic(@IBAN String iban) {
        // empty
    }

    void lenient(@IBAN(check = Check.LENIENT) String iban) {
        // empty
    }

    void sepa(@IBAN(type = IbanType.SEPA, check = Check.LENIENT) String iban) {
        // empty
    }

    void noSepa(@IBAN(type = IbanType.NO_SEPA, check = Check.LENIENT) String iban) {
        // empty
    }

    @ParameterizedTest
    @CsvSource({
            "DE75512108001245126199", "AL35202111090000000001234567", "AT483200000012345864", "IE64IRCE92050112345678", "NL02ABNA0123456789",
            "CH5604835012345678009", "NL02ABNA0123456789", "DE56683519761116232594"
    })
    void isValidWithLenient(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("lenient", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertTrue(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({
            "DE75512108001245126199", "AL35202111090000000001234567", "AT483200000012345864", "IE64IRCE92050112345678", "NL02ABNA0123456789",
            "CH5604835012345678009", "NL02ABNA0123456789"
    })
    void isValidWithPragmatic(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("pragmatic", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertTrue(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({
            "DE75512108001245126199", "AT483200000012345864", "CH5604835012345678009"
            //, "NL02ABNA0123456789"
    })
    void isValidWithStrict(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("strict", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertTrue(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({
            "DE75512108001245126199", "AT483200000012345864", "IE64IRCE92050112345678", "NL02ABNA0123456789",
            "CH5604835012345678009", "NL02ABNA0123456789"
    })
    void isValidWithSepa(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("sepa", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertTrue(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({
            "AL35202111090000000001234567", "UA903052992990004149123456789"
    })
    void isValidWithNoSepa(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("noSepa", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertTrue(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({"DE56683519761116232594"
    })
    void isInvalidWithPragmatic(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("pragmatic", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertFalse(ibanValidator.isValid(iban, null));
    }

    @ParameterizedTest
    @CsvSource({"DE56683519761116232594"
    })
    void isInvalidWithStrict(String iban) throws NoSuchMethodException {
        IbanValidator ibanValidator = new IbanValidator();
        ibanValidator.initialize((IBAN) getClass().getDeclaredMethod("pragmatic", String.class).getParameterAnnotations()[0][0]);
        Assertions.assertFalse(ibanValidator.isValid(iban, null));
    }
}