package de.schegge.bank.validator;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class BicValidatorTest {

    void lenient(@BIC(check = Check.LENIENT) String bic) {
        // empty
    }

    void pragmatic(@BIC String bic) {
        // empty
    }

    void strict(@BIC(check = Check.STRICT) String bic) {
        // empty
    }

    @ParameterizedTest
    @CsvSource({
            "BYLADEM1001", "INGDDEFF", "BELADEBE", "CMCIDEDD", "RLNWATWW", "BKAUATWW", "KBSGCH22", "BCVLCH2L"
    })
    void isValidWithStrict(String bic) throws NoSuchMethodException {
        BicValidator bicValidator = new BicValidator();
        bicValidator.initialize((BIC)getClass().getDeclaredMethod("strict", String.class).getParameterAnnotations()[0][0]);
        assertTrue(bicValidator.isValid(bic, null));
    }

    @ParameterizedTest
    @CsvSource({"EASYATW1", "INGDDEEE"})
    void isInvalidWithStrict(String bic) throws NoSuchMethodException {
        BicValidator bicValidator = new BicValidator();
        bicValidator.initialize((BIC)getClass().getDeclaredMethod("strict", String.class).getParameterAnnotations()[0][0]);
        assertFalse(bicValidator.isValid(bic, null));
    }

    @ParameterizedTest
    @CsvSource({
            "BYLADEM1001", "INGDDEFF", "BELADEBE", "CMCIDEDD",
            "OPSKATWW", "EASYATW1", "RLNWATWW", "BKAUATWW", "KBSGCH22", "BCVLCH2L"
    })
    void isValidWithLenient(String bic) throws NoSuchMethodException {
        BicValidator bicValidator = new BicValidator();
        bicValidator.initialize((BIC)getClass().getDeclaredMethod("lenient", String.class).getParameterAnnotations()[0][0]);
        assertTrue(bicValidator.isValid(bic, null));
    }

    @ParameterizedTest
    @CsvSource({
            "BYLADEM10011", "INGDDEF","RLNWATWWW", "BKAUATW"
    })
    void isInvalidWithLenient(String bic) throws NoSuchMethodException {
        BicValidator bicValidator = new BicValidator();
        bicValidator.initialize((BIC)getClass().getDeclaredMethod("lenient", String.class).getParameterAnnotations()[0][0]);
        assertFalse(bicValidator.isValid(bic, null));
    }
}